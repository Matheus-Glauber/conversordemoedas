import { ConversaoResponse } from "./../models/conversao-response.model";
import { Injectable } from "@angular/core";
import { Conversao } from "../models/conversao.model";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class ConversorService {
  private readonly BASE_URL =
    "https://data.fixer.io/api/latest?access_key=87387b1469ef9f84a21325c767d8da30";

  constructor(private http: HttpClient) {}

  /**
   * Realiza a chamada para a API de conversão de moedas.
   *
   * @param conversao
   * @returns Observable<ConversaoResponse>
   */
  converter(conversao: Conversao): Observable<ConversaoResponse> {
    let params = `&base=${conversao.moedaDe}&symbols=${conversao.moedaPara}`;

    return this.http.get<ConversaoResponse>(this.BASE_URL + params);
  }

  /**
   * Retorna a cotação PARA dado uma response.
   *
   * @param conversaoResponse
   * @param conversao
   * @returns number
   */
  cotacaoPara(
    conversaoResponse: ConversaoResponse,
    conversao: Conversao
  ): number {
    if (conversaoResponse === undefined) {
      return 0;
    }
    return conversaoResponse.rates[conversao.moedaPara];
  }

  /**
   * Retorna a cotação DE dado uma response.
   *
   * @param conversaoResponse
   * @param conversao
   * @returns string
   */
  cotacaoDe(
    conversaoResponse: ConversaoResponse,
    conversao: Conversao
  ): string {
    if (conversaoResponse === undefined) {
      return "0";
    }

    return (1 / conversaoResponse.rates[conversao.moedaPara]).toFixed(4);
  }

  /**
   * Retorna a data da cotação dado uma response.
   *
   * @param conversaoResponse
   * @returns string
   */
  dataCotacao(conversaoResponse: ConversaoResponse): string {
    if (conversaoResponse === undefined) {
      return "";
    }

    return conversaoResponse.date;
  }
}
