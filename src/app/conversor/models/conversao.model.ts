/**
 * Entidade que modela a Conversao da entidade moeda.
 *
 * @author Matheus Glauber <glauber.jordao1995@gmail.com>
 */
export class Conversao {
  constructor(
    public moedaDe?: string,
    public moedaPara?: string,
    public valor?: number
  ) {}
}
