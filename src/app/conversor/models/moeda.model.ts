/**
 * Entidade que modela a moeda.
 *
 * @author Matheus Glauber <glauber.jordao1995@gmail.com>
 * @param sigla - String que representa a sigla da moeda.
 * @param descricao - String detalhando qual a moeda selecionada.
 */
export class Moeda {
  constructor(public sigla?: string, public descricao?: string) {}
}
