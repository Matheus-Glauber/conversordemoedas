/**
 * Entidade que modela a estrutura de dados que serão consumidas do serviço.
 *
 * @author Matheus Glauber <glauber.jordao1995@gmail.com>
 */
export class ConversaoResponse {
  constructor(public base: string, public date: string, public rates: any) {}
}
