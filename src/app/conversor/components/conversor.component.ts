import { Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Moeda } from "../models/moeda.model";
import { Conversao } from "../models/conversao.model";
import { ConversaoResponse } from "../models/conversao-response.model";
import { MoedaService } from "../service/moeda.service";
import { ConversorService } from "../service/conversor.service";

@Component({
  selector: "app-conversor",
  templateUrl: "./conversor.component.html",
  styleUrls: ["./conversor.component.css"]
})
export class ConversorComponent implements OnInit {
  private moedas: Moeda[];
  private conversao: Conversao;
  private possuiErro: boolean;
  private conversaoResponse: ConversaoResponse;

  @ViewChild("conversaoForm", { static: true }) conversaoForm: NgForm;

  constructor(
    private moedaService: MoedaService,
    private conversorService: ConversorService
  ) {}

  ngOnInit() {
    this.moedas = this.moedaService.listarTodas();
    this.init();
  }

  /**
   * Efetua a chamada para a conversão dos valores.
   * @returns void
   */
  init(): void {
    this.conversao = new Conversao("USD", "BRL", null);
    this.possuiErro = false;
  }

  /**
   * Efetua a chamada para a conversão dos valores.
   * @returns void
   */
  converter(): void {
    try {
      if (this.conversaoForm.form.valid) {
        this.conversorService.converter(this.conversao).subscribe(
          response => {
            this.conversaoResponse = response;
          },
          error => {
            this.possuiErro = true;
          }
        );
      }
    } catch (error) {
      console.log(error);
    }
  }
}
