import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { ConversorComponent } from "./components/conversor.component";
import { MoedaService } from "./service/moeda.service";
import { ConversorService } from "./service/conversor.service";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { NumeroDirective } from './directives/numero.directive';

@NgModule({
  declarations: [ConversorComponent, NumeroDirective],
  imports: [CommonModule, FormsModule, HttpClientModule],
  exports: [ConversorComponent],
  providers: [MoedaService, ConversorService]
})
export class ConversorModule {}
