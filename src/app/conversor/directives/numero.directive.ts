import { Directive } from "@angular/core";
import { HostListener } from "@angular/core";
import { ElementRef } from "@angular/core";

import { NG_VALUE_ACCESSOR } from "@angular/forms";
import { ControlValueAccessor } from "@angular/forms";

@Directive({
  selector: "[numero]",
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: NumeroDirective,
      multi: true
    }
  ]
})
export class NumeroDirective implements ControlValueAccessor {
  onChange: any;
  onTouched: any;
  _renderer: any;
  _elementRef: any;

  constructor(private el: ElementRef) {}

  /**
   * Implementa evento de keyup para o elemento da diretiva.
   * keyup - Sempre que pressionar e soltar uma tecla o método é executado.
   * keydown - o método é chamado sempre que pressionar a tecla.
   *
   * @param $event
   */
  @HostListener("keyup", ["$event"])
  onKeyUp($event: any) {
    //Obtém valor do campo de texto (target é o campo input de texto)
    let valor = $event.target.value;
    //Pega a posição decimal depois do ponto
    let posDecimais = valor.indexOf(".");

    valor = valor.replace(/[\D]/g, "");

    if (posDecimais > 0) {
      valor = valor.substr(0, posDecimais) + "." + valor.substr(posDecimais);
    }

    $event.target.value = valor;
    this.onChange(valor);
  }

  /**
   * Registra uma função a ser chamada para atualizar o valor na model.
   *
   * @param fn
   */
  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  /**
   * Registra função a ser chamada para atualizar valor na model para evento touched.
   *
   * @param fn
   */
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  /**
   * Obtém o valor contido na model.
   *
   * @param value
   */
  writeValue(value: any): void {
    this.el.nativeElement.value = value;
  }

  /**
   * Habilidade ou desabilita o elemento DOM apropriado, quando a função controle é alterado.
   *
   * @param isDisabled
   */
  setDisabledState?(isDisabled: boolean): void {
    this._renderer.setProperty(
      this._elementRef.nativeElement,
      "disabled",
      isDisabled
    );
  }
}
